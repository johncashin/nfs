import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    customer: {
      name: {
        title: '',
        fname: null,
        sname: null,
      },
      dob: {
        d: '',
        m: '',
        y: null
      }
    },
    jointCustomer: {
      name: {
        title: 'null',
        fname: null,
        sname: null,
      },
      dob: {
        d: '',
        m: '',
        y: null
      }
    },
    search: {
      life2: false,
      gender1: null,
      gender2: null,
      smoker1: null,
      smoker2: null,
      declaration1: false,
      declaration2: false
    }
  },
  mutations: {
    SET_SEARCH (state, payload) {
      state.search = { ...state.search, ...payload };
    },
    SET_CUSTOMER (state, payload) {
      state.customer[payload.property] = { ...state.customer[payload.property], ...payload.value }
    }
  },
  getters: {
    search (state) {
      return state.search;
    },
    customer (state) {
      return state.customer;
    },
    jointCustomer (state) {
      return state.jointCustomer;
    }
  }
});

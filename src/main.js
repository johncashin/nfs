import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import i18n from './i18n';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faUser, faSmokingBan, faSmoking, faSave, faPhoneAlt, faCommentAltLines, faUserFriends, faCaretDown, faCircle as fasCircle } from '@fortawesome/pro-solid-svg-icons';
import { faCheck, faCircle, faQuestionCircle } from '@fortawesome/pro-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faUser, faSmokingBan, faSmoking, faSave, faPhoneAlt, faCommentAltLines, faQuestionCircle, faUserFriends, faCheck, faCaretDown, faCircle, fasCircle)
Vue.component('FontAwesomeIcon', FontAwesomeIcon);

import VueTippy, { TippyComponent } from "vue-tippy";
import "tippy.js/themes/light.css";
import "tippy.js/themes/light-border.css";

Vue.use(VueTippy, {
  theme: 'light'
});
Vue.component("tippy", TippyComponent);

Vue.use(VueTippy, {
  directive: "tippy",
  flipDuration: 0,
  popperOptions: {
    modifiers: {
      preventOverflow: {
        enabled: false
      }
    }
  }
});
Vue.component("TippyComponent", TippyComponent);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
